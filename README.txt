eVENTeria Drupal Module - README
--------------------------------
The eVENTeria Module delivers an API to the venteria.com Website. For handling the XML stuff, the xml.php from Keith Devens (http://keithdevens.com/software/phpxml) is used.
The module consits of several modules:

- eventeria.module
  - The basic module.
  - It delivers the API for all other eventeria* modules.

- eventeria_event.module
  - Every content type that uses the event.module can be used to create events on venteria.com


current problems:
-----------------
The venteria.com API has problems with updating existing events. So for now you can only create and delte events on venteria.com